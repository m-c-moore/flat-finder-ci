# Flat Finder

This repository used to handle the search pipelines for the [Flat Finder project](https://gitlab.com/m-c-moore/flat-finder), but has been deprecated in favour of CircleCI. Please refer to the main project or the [website](https://m-c-moore.gitlab.io/flat-finder) for more information.
